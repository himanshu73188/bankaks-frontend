### What is this repository for? ###

* Quick summary

    Resize & compress image before uploading to server.

* Version 1.0.0.0

### Problem Statement ###

    A user on the Bankaks mobile platform wants to upload a picture on their profile for
    which he is allowed to take their picture using their mobile phone. The picture that
    the phone camera takes is directly uploaded on a Blob storage platform. A typical
    image captured by a cell-phone camera is 3 to 9 Mb. Over the slow networks, loading
    such a big image leads to bad UX and affects the user retention on the application.
    In order to reduce the image load time, an image uploaded by the User should be
    saved on the cloud in multiple sizes.

    For example, a user takes an image from their phone which is 4290 x 2800 px in
    size. The user taps on the upload button in the mobile application and the image is
    reduced to 214.5 x 140 px and 429 x 280 px before uploading all three versions on
    the cloud.

### Solution Statement ###

    The ng2-img-max module is just what you need to solve above problem. It will limit the dimension of uploaded images directly on the frontend before even uploading anything to server.
  
* Installation 

    `npm install ng2-img-max blueimp-canvas-to-blob`

* .angular-cli.json 

    `...
    "scripts": [
    "../node_modules/blueimp-canvas-to-blob/js/canvas-to-blob.min.js"
    ],`

* Now let’s import the module in you app module or into a feature module:

    `import { Ng2ImgMaxModule } from 'ng2-img-max';`

* And finally, the ng2-img-max service can be imported and injected in any component

    `import { Ng2ImgMaxService } from 'ng2-img-max';`


### How do I get set up? ###

* npm install
* ng serve
* open http://localhost:4200/

### Who do I talk to? ###

* himanshu73188@gmail.com
