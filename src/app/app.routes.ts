import { Routes, RouterModule } from "@angular/router";

// App Component.
// -------------
import { LoginComponent } from "./login/login.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { UserSettingsComponent } from "./user-settings/user-settings.component";


export const router: Routes = [

    { path: '', redirectTo: 'user-settings', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'user-settings', component: UserSettingsComponent},
    { path: '**', component: PageNotFoundComponent }

];

export const routes = RouterModule.forRoot(router);
