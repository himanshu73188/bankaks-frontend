import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  private data           = new BehaviorSubject<any>(null);
  
  constructor(
    private api: ApiService ) { }

  uploadImage(fileToUpload: any, id) {
    
    var formData: FormData = new FormData();
    for (const file of fileToUpload) {
     formData.append("file", file)
    }
    // console.log(formData.getAll('file'));
    // return this.api.post('v1/users/user/' + id + '/uploadImage', formData);

    // fake response
    this.data.next({'status' : 'success' });
    return this.data.asObservable();
  }

}
