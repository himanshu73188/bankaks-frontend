import { Component, OnInit } from '@angular/core';
import { UiService } from '../shared/ui.service';
import { UploadService } from '../shared/upload.service';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { combineLatest } from 'rxjs';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss']
})
export class UserSettingsComponent implements OnInit {

  // variables
  uploadedImage1: Blob;
  uploadedImage2: Blob;

  imageOriginalLink: any;
  imageLink1: any;
  imageLink2: any;

  constructor(
    private ui: UiService,
    private upload: UploadService,
    private ng2ImgMax: Ng2ImgMaxService,
    private auth: AuthService
  ) { }

  ngOnInit() {

  }

  onImageChange(event) {
    
    this.ui.showSnackbar("Uploading...please wait", "pending");

    let image                       = event.target.files[0];
    const reader                    = new FileReader();
    reader.readAsDataURL(image); 
    reader.onload                   = () => {

      // set original image link
      this.imageOriginalLink        = reader.result;
      const img                     = new Image();
      img.src                       = reader.result as string;

      img.onload                    = () => {

        // remove old imageLink1 & imageLink2 after uploading new image
        this.imageLink1             = null;
        this.imageLink2             = null;

        // set width & height
        const height                = img.naturalHeight;
        const width                 = img.naturalWidth;

        // resize image
        let image1                  = this.ng2ImgMax.resizeImage(image, width/10, height/10);
        let image2                  = this.ng2ImgMax.resizeImage(image, width/20, height/20);
        
        combineLatest([image1, image2])
        .subscribe(
          results => {
          
            this.uploadedImage1     = results[0];
            this.uploadedImage2     = results[1];
            
            // set imageLink1 src
            const reader1           = new FileReader();
            reader1.readAsDataURL(this.uploadedImage1); 
            reader1.onload          = () => { 
              this.imageLink1       = reader1.result as string;
            }

            // set imageLink2 src
            const reader2           = new FileReader();
            reader2.readAsDataURL(this.uploadedImage2); 
            reader2.onload          = () => { 
              this.imageLink2       = reader2.result as string;
            }

          },
          error => {
            this.ui.showSnackbar("API error", "failure");
          }
        );

      };
    }
    
  }

  uploadFile() {
    // upload to server

    this.ui.showSnackbar("Uploading...please wait", "pending");

    let fileToUpload             = [this.uploadedImage1, this.uploadedImage2];
    let userId                   = 0;

    this.upload.uploadImage(fileToUpload, userId)
      .subscribe(data => {
        console.log(data);
        if(data.status == "success") {
          this.auth.setUserPhoto(this.imageLink1);
          this.ui.showSnackbar("Image successfully uploaded", "success");
        } else {
          this.ui.showSnackbar("API error", "failure");
        }
    }, err => {
      this.ui.showSnackbar("API error", "failure");
    });

  }

}
