import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

// Angular Material
import { AppMaterialModule } from './app-material.module';

// Services
import { ApiService } from './shared/api.service';
import { AuthService } from './shared/auth.service';
import { UiService } from './shared/ui.service';

// Routes
import { routes } from './app.routes';

// App Components
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserSettingsComponent } from './user-settings/user-settings.component';

import { Ng2ImgMaxModule } from 'ng2-img-max';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PageNotFoundComponent,
    UserSettingsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    routes,
    Ng2ImgMaxModule
  ],
  providers: [ApiService, AuthService, UiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
